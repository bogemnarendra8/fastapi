import os
import hashlib
from xml.dom import ValidationErr
from pydantic import ValidationError
import sqlalchemy
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import column, create_engine
from sqlalchemy import String,Column, Integer, ForeignKey
from sqlalchemy.future import Engine
from sqlalchemy.orm import validates
from sqlalchemy.orm import Session
import re



engine: Engine= create_engine("sqlite:/// ./main.db")
session=Session(engine)
Base=declarative_base()
metadata=sqlalchemy.MetaData()

username_validator = re.compile(r"\A[A-Z0-9_.]{5,128}\Z", re.IGNORECASE)
email_validator = re.compile(r'([A-Za-z0-9]+[.-_])*[A-Za-z0-9]+@[A-Za-z0-9-]+(\.[A-Z|a-z]{2,})+')

class Posts(Base):
    __tablename__="posts"
    id : int =Column(Integer(),primary_key=True)
    description: str = Column(String(540))
    content : str = Column(String(600))
    author : str = Column(String(64),ForeignKey("users.username"))


class Users(Base):
    __tablename__ = "users"
    username: str=Column(String(64),primary_key=True)
    email : str = Column(String(256),unique=True)
    salt : str = Column(String(128))
    hashed_password : str = Column(String(128))
    token : str = Column(String(128))


    @validates("username")
    def validate_username(self, key, username):
        if username_validator.fullmatch(username):
            return username
        else:
            raise ValidationError("invalid username")
    
    # @validates("email")
    # def validate_username(self,key,email):
    #     if email_validator.fullmatch(email):
    #         return email
    #     else:
    #         raise ValueError("invalid email")

    def gen_hash(self, password: str):
        self.salt = os.urandom(64).hex()
        hash = hashlib.pbkdf2_hmac("sha512", password.encode("utf-8"), self.salt.encode("utf-8"), 100000)
        self.hashed_password = hash.hex()

    
    def authenticate(self, password: str,salt: str,check_password) -> bool:
        # if self.hash_algorithm == "pbkdf2-hmac-sha512":
        new_hash = hashlib.pbkdf2_hmac("sha512", password.encode("utf-8"), salt.encode("utf-8"), 100000).hex()
        if new_hash ==check_password:
            return True
        else:
            return False

Base.metadata.drop_all(engine)
Base.metadata.create_all(engine)



