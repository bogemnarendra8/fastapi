from fastapi import FastAPI
from pydantic import BaseModel
from model import Users,Posts,session
import os

app=FastAPI()

class users(BaseModel):
    username: str
    email: str
    password: str

class posts(BaseModel):
    id : int
    description : str
    content : str
    author : str





@app.get("/")
def root():
    return {
        "message":"http://127.0.0.1:8000/.docs"
    }

@app.post("/users/signup")
def user_add(user_to_add: users):
    db_user=session.query(Users).filter(Users.username==user_to_add.username).first()
    try:
        if db_user==None:
            user=Users(username=user_to_add.username,email=user_to_add.email)
            user.gen_hash(user_to_add.password)
            session.add(user)
            session.commit()
            return{
                "username":"succesful"
                }
        else:
            return{
                "message":"username  already taken"
            }
    except ValueError as err:
        return {
            "message":str(err)
        }


@app.post("/user/login")
def user_login(user_to_login:users):
    db_user=session.query(Users).filter(Users.username==user_to_login.username).first()
    if db_user==None:
        return {
            "message":"login failed"
        }
    elif db_user.token==None:
        user=Users(username=user_to_login.username,email=user_to_login.email)
        if user.authenticate(user_to_login.password,db_user.salt,db_user.hashed_password):
            db_user.token = os.urandom(64).hex()
            session.commit()
            return {
                "message":"login success"
            }
    else:
        return{
            "message": f"user {user_to_login.username} already loggedin"

        }
        
        
    


@app.post("/user/logout")
def user_logout(user_to_logout: users):
    #to find the user to logout
    user_logout=session.query(Users).filter(Users.username==user_to_logout.username).first()
    if user_logout.token:
        user_logout.token=None
        session.commit()
        return{
            "message":"logout success"
        }
    else:
        return{
            "message":"login or signup required"
        }


@app.post("/post/post")
def add_post(post: posts):
    db_post=session.query(Users).filter(Posts.id==post.id).first()
    if db_post==None:
        post=Posts(**post.dict())
        session.add(post)
        session.commit()
        return{
            "message":"post added succesfully"
        }
    else:
        return{
            "message":f"post with id {post.id} already taken"
        }

@app.get("/post/{post_id}")
def get_post(post_id:int): 
    req_post=session.query(Posts).filter(Posts.id==post_id).first()
    
     
    if req_post==None:
        return {
            "message":f"no post with this id {post_id}"
        }
    else:
        return {
            "id": req_post.id,
            "description": req_post.description,
            "content" : req_post.content
        }

# @app.get("/post/all")
# def get_all():

    